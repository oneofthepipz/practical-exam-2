from django.conf import settings


ALL_SECTION_NAME = getattr(settings, "blogs_ALL_SECTION_NAME", "all")
SECTIONS = getattr(settings, "blogs_SECTIONS", [])

DEFAULT_MARKUP_CHOICE_MAP = {
    "creole": {"label": "Creole", "parser": "blogs.parsers.creole_parser.parse"},
    "markdown": {"label": "Markdown", "parser": "blogs.parsers.markdown_parser.parse"}
}
MARKUP_CHOICE_MAP = getattr(settings, "blogs_MARKUP_CHOICE_MAP", DEFAULT_MARKUP_CHOICE_MAP)
MARKUP_CHOICES = [
    (key, MARKUP_CHOICE_MAP[key]["label"])
    for key in MARKUP_CHOICE_MAP.keys()
]